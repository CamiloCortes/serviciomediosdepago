package co.edu.javeriana.aes.swaggerconfig;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public Docket api() {

		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("co.edu.javeriana.*"))
				.paths(PathSelectors.any())
				.build();
	}
	

			private ApiInfo apiInfo() {
			    return new ApiInfoBuilder()
			    .title("manejo de servicios rest metodos de pago")
			    .description("esta pagina lista los metodos disponibles para realizar el pago de facturas ")
			    .version("1.0-SNAPSHOT")
			    .build();
			}
}
