package co.edu.javeriana.aes.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.edu.javeriana.aes.dto.Metodopago;
@Service
public class MetodoPagoServiceImpl implements MetodoPagoService{

	@Override
	public List<Metodopago> listarMetosDePagoDisponibles() {
		List<Metodopago> listaMediosPago = new ArrayList<>();
		listaMediosPago.add(new Metodopago(1, "1A", "PSE", "Tipo de pago que se implementa atravez del debito bancario por el boton de pagos PSE"));
		listaMediosPago.add(new Metodopago(2, "2A", "Tarjeta de credito", "Tipo de pago que se implementa a travez tarjeta de credito"));
		listaMediosPago.add(new Metodopago(3, "3A", "crediPuntos", "Tipo de pago que se implementa a travez de descuento de puntos acumulados del cliente con la entidad"));
		return listaMediosPago;
	}


}
