package co.edu.javeriana.aes.dto;

public class Metodopago {

	public int idPago;
	public String codTipoPago;
	public String nombreTipoPago;
	public String descripcionTipoPago;
	
	public Metodopago(int idPago, String codTipoPago, String nombreTipoPago, String descripcionTipoPago) {
		super();
		this.idPago = idPago;
		this.codTipoPago = codTipoPago;
		this.nombreTipoPago = nombreTipoPago;
		this.descripcionTipoPago = descripcionTipoPago;
	}
	public int getIdPago() {
		return idPago;
	}
	public void setIdPago(int idPago) {
		this.idPago = idPago;
	}

	public String getCodTipoPago() {
		return codTipoPago;
	}

	public void setCodTipoPago(String codTipoPago) {
		this.codTipoPago = codTipoPago;
	}
	public String getNombreTipoPago() {
		return nombreTipoPago;
	}
	public void setNombreTipoPago(String nombreTipoPago) {
		this.nombreTipoPago = nombreTipoPago;
	}
	public String getDescripcionTipoPago() {
		return descripcionTipoPago;
	}
	public void setDescripcionTipoPago(String descripcionTipoPago) {
		this.descripcionTipoPago = descripcionTipoPago;
	}

	}
