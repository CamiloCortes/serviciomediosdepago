package co.edu.javeriana.aes.service;

import java.util.List;

import co.edu.javeriana.aes.dto.Metodopago;

public interface MetodoPagoService {

	public List<Metodopago> listarMetosDePagoDisponibles();
}
