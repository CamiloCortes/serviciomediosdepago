package co.edu.javeriana.aes.metodospago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "co.edu.javeriana.*"} )
public class MetodosPagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetodosPagoApplication.class, args);
	}

}
