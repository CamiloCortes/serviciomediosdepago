package co.edu.javeriana.aes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.javeriana.aes.dto.Metodopago;
import co.edu.javeriana.aes.service.MetodoPagoService;;

@RestController
public class MetodosPagoController {
	
	@Autowired
	MetodoPagoService pagosService;
	
	@GetMapping("/listarTiposPago")
	public List<Metodopago> retornarPagos(){
		
		return pagosService.listarMetosDePagoDisponibles(); 
	} 
	
}
