# Build Stage
FROM maven:3.6.1-jdk-8 AS builder
WORKDIR /mediosPagoService
COPY pom.xml .
COPY ./src ./src
RUN mvn clean package

# Run Stage
FROM openjdk:8u201-jdk-alpine3.9
LABEL maintainer="Camilo Andres Cortes - camilo-cortesf@javeriana.edu.co"
WORKDIR WORKSPACE /mediosPagoService
COPY --from=builder /mediosPagoService/target/metodos-pago-0.0.1-SNAPSHOT.jar .
ENTRYPOINT java -Djava.net.preferIPv4Stack=true -jar metodos-pago-0.0.1-SNAPSHOT.jar